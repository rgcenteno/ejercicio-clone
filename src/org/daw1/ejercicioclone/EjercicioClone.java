/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package org.daw1.ejercicioclone;

import org.daw1.ejercicioclone.classes.Empresa;
import org.daw1.ejercicioclone.classes.Sector;
import org.daw1.ejercicioclone.classes.Ubicacion;

/**
 *
 * @author Rafael González Centeno
 */
public class EjercicioClone {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws CloneNotSupportedException {
        Empresa e1 = new Empresa("Empresa 1", Sector.TERCIARIO);
        Ubicacion u1 = new Ubicacion("As Neves", "Calle test", 1);
        Ubicacion u2 = new Ubicacion("Ponteareas", "Calle test2", 2);
        Ubicacion u3 = new Ubicacion("A Cañiza", "Calle test3", 3);
        e1.addUbicacion(u1);
        e1.addUbicacion(u2);
        e1.addUbicacion(u3);
        System.out.println(e1);
        
        Empresa e2 = (Empresa)e1.clone();
        e2.get(0).setLocalidad("Vigo");
        e2.removeUbicacion(u1);
        e2.setSector(Sector.SECUNDARIO);
        System.out.println(e1);
    }
    
}
