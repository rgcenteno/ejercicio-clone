/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.daw1.ejercicioclone.classes;

import java.util.List;

/**
 *
 * @author Rafael González Centeno
 */
public class Empresa{
    private String nombre;
    private Sector sector;
    private java.util.List<Ubicacion> ubicaciones;

    public Empresa(String nombre, Sector sector) {
        this.nombre = nombre;
        this.sector = sector;
        this.ubicaciones = new java.util.ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }
    
    public boolean addUbicacion(Ubicacion u){
        return this.ubicaciones.add(u);
    }
    
    public boolean removeUbicacion(Ubicacion u){
        return this.ubicaciones.remove(u);
    }

    @Override
    public String toString() {
        return "Empresa{" + "nombre=" + nombre + ", sector=" + sector + ", ubicaciones=" + ubicaciones + '}';
    }

    public Ubicacion get(int index){
        return this.ubicaciones.get(index);
    }
    
    public int getNumUbicaciones(){
        return this.ubicaciones.size();
    }
    
    
}
