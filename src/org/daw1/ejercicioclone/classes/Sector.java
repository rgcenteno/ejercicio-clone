/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package org.daw1.ejercicioclone.classes;

/**
 *
 * @author Rafael González Centeno
 */
public enum Sector {
    PRIMARIO, SECUNDARIO, TERCIARIO;
    
    public static Sector of(int eleccion) {
        java.util.Objects.checkIndex(eleccion - 1, Sector.values().length);
        return Sector.values()[eleccion - 1];
    }
}
