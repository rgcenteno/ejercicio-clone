/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.ejercicioclone.classes;

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author rgcenteno
 */
public class EmpresaNGTest {
    
    private static Empresa e1, e2;
    
    public EmpresaNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        System.out.println("********************************SETUP***********************");
        e1 = new Empresa("Empresa 1", Sector.TERCIARIO);
        Ubicacion u1 = new Ubicacion("As Neves", "Calle test", 1);
        Ubicacion u2 = new Ubicacion("Ponteareas", "Calle test2", 2);
        Ubicacion u3 = new Ubicacion("A Cañiza", "Calle test3", 3);
        e1.addUbicacion(u1);
        e1.addUbicacion(u2);
        e1.addUbicacion(u3);        
        
        e2 = e1.clone();
        
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of getNombre method, of class Empresa.
     */
    @Test
    public void testClone1() {        
        System.out.println("testClone1");
        e2.setNombre("Test 2");
        String expResult = "Empresa 1";
        String result = e1.getNombre();
        assertEquals(result, expResult);
    }

    /**
     * Test of getNombre method, of class Empresa.
     */
    @Test
    public void testClone2() {        
        System.out.println("testClone2");
        e2.get(0).setLocalidad("Vigo");        
        String expResult = "As Neves";
        String result = e1.get(0).getLocalidad();
        assertEquals(result, expResult);
    }
    
    /**
     * Test of getNombre method, of class Empresa.
     */
    @Test
    public void testClone3() {        
        System.out.println("testClone3");        
        Ubicacion u1 = new Ubicacion("As Neves", "Calle test", 1);
        e2.removeUbicacion(u1);
        int expResult = 3;
        int result = e1.getNumUbicaciones();
        assertEquals(result, expResult);
    }
    
    /**
     * Test of getNombre method, of class Empresa.
     */
    @Test
    public void testClone4() {      
        System.out.println("testClone4");        
        Ubicacion u1 = new Ubicacion("Ourense", "Calle test", 5);
        Ubicacion u2 = new Ubicacion("Leboriz", "Calle test", 5);
        e2.addUbicacion(u1);
        e2.addUbicacion(u2);
        int expResult = 3;
        int result = e1.getNumUbicaciones();
        assertEquals(result, expResult);
    }
    
    @Test
    public void testClone5() {              
        System.out.println("testClone5");        
        e2.setSector(Sector.SECUNDARIO);
        Sector expResult = Sector.TERCIARIO;
        Sector result = e1.getSector();
        assertEquals(result, expResult);
    }
    
}
